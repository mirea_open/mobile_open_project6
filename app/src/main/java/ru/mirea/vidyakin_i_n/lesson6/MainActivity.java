package ru.mirea.vidyakin_i_n.lesson6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.mirea.vidyakin_i_n.lesson6.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    EditText editText1;
    EditText editText2;
    EditText editText3;
    ActivityMainBinding binding;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        button = binding.button;
        editText1 = binding.editTextTextPersonName;
        editText2 = binding.editTextTextPersonName2;
        editText3 = binding.editTextTextPersonName3;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences("mirea_settings", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("GROUP", "БСБО-01-20");
                editor.putInt("NUMBER", 7);
                editor.putString("FILM", "Lord of the rings");
                editor.apply();
                editText1.setText(sharedPref.getString("GROUP", ""));
                editText2.setText(String.valueOf(sharedPref.getInt("NUMBER", -1)));
                editText3.setText(sharedPref.getString("FILM", ""));
            }
        });
    }
}