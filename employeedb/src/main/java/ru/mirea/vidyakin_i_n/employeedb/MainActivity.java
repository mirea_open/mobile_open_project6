package ru.mirea.vidyakin_i_n.employeedb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Database;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppDatabase db = App.getInstance().getDatabase();
        SuperheroDao superheroDao= db.superheroDao();

        Superhero superhero = new Superhero();
        superhero.name = "Flash";
        superhero.age = 32;
        superhero.power = "Superspeed";

        superheroDao.insert(superhero);

        List<Superhero> superheroList = superheroDao.getAll();
        superhero = superheroDao.getById(1);
        superhero.age++;
        superheroDao.update(superhero);
        Log.d("SUBD", superhero.name + " " + superhero.age + " " + superhero.power);
    }
}